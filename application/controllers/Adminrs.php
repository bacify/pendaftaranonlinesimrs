<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Adminrs extends CI_Controller {

public function __construct(){
    parent::__construct();
    $this->load->library('Templateadmin');
      $this->load->model('madmin');
    
  		 	  
  }

  public function checklogin(){
  	if($this->session->userdata('admin')==NULL){
   			redirect('/adminrs', 'refresh');
   		}
  }

  public function index(){
  	 
  	$this->login();
  }

  public function login(){
  	$submit=$this->input->post('submit');
  	$user=$this->input->post('username');
  	$password=$this->input->post('password');

  	if($submit!=null){
  		$v['username']=$user;
  		$v['password']=$password;
  		$login=$this->madmin->getadmin($v);
  		if(count($login)>0){

  			$this->session->set_userdata(array('admin'=>1,
  												'username'=>$login[0]->username));
  			redirect(base_url('adminrs/jadwal_poli'));
  		}else{
  			$this->session->set_flashdata('pesan','login Gagal.');
  		}
  	}

  	$this->templateadmin->load('login');
  }
  public function logout(){
  	 
		$this->session->sess_destroy();
		redirect(base_url('adminrs'));
	 
  }

  public function jadwal_poli(){
  	$this->checklogin();
  	$hari=array('1'=>'Senin',
  				'2'=>'Selasa',
  				'3'=>'Rabu',
  				'4'=>'Kamis',
  				'5'=>'Jumat',
  				'6'=>'Sabtu'
  					);
  	$layanan=$this->madmin->getlayanan();
  	$data['day']=$hari;
  	$data['layanan']=$layanan;
  	$this->templateadmin->load('jadwal_poli',$data);
  }

  public function view_jadwal(){

    // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $jadwal = $this->madmin->get_jadwal();

          $data = array();
          $x=1;


          foreach($jadwal as $r) {
           		
           		if($r->day==1)
           			$r->hari='Senin';
           		else if($r->day==2)
           			$r->hari='Selasa';
           		else if($r->day==3)
           			$r->hari='Rabu';
           		else if($r->day==4)
           			$r->hari='Kamis';
           		else if($r->day==5)
           			$r->hari='Jumat';
           		else if($r->day==6)
           			$r->hari='Sabtu';
           		else  
           			$r->hari=' ';
           		 
               $data[] = array(
                    $x++,
                    $r->hari,
                    $r->nama_layanan,                     
                    '<a href="javascript:;" class="btn btn-info btn-xs item_edit" data="'.$r->id_jadwal.'">Edit</a> '.
                    '<a href="javascript:;" class="btn btn-danger btn-xs item_hapus" data="'.$r->id_jadwal.'">Hapus</a>'
                     
                     
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($jadwal),
                 "recordsFiltered" => count($jadwal),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
  }
  public function simpan_jadwal(){
  	$id=$this->input->post('id');
  	$day=$this->input->post('hari');
  	$layanan=$this->input->post('poli');

  	$pesan='';
  	if($id==null){
  		$b['id_jadwal']=$id;
  		$c1['day']=$day;
  		$c1['layanan_id']=$layanan;
  		$a=$this->madmin->get_jadwal($c1);
  		if(count($a)>0){
  			$pesan='Data Sudah Ada';
  		}{
  			$this->madmin->insert_jadwal($c1);
  			$pesan='Jadwal Berhasil Di Tambahkan';
  		}
  	}else{
  		$b['id_jadwal']=$id;
  		$c1['day']=$day;
  		$c1['layanan_id']=$layanan;
  		$this->madmin->update_jadwal($b,$c1);
  		$pesan='Jadwal berhasil Di Update';
  	}
  	echo json_encode($pesan);

  }
  public function get_jadwal(){
    $id=$this->input->get('id');
    $c['id_jadwal']=$id;
    $r=$this->madmin->get_jadwal($c);
    echo json_encode($r[0]);

  }
  public function hapus_jadwal(){
    $id=$this->input->post('id');
    $c['id_jadwal']=$id;
    $r=$this->madmin->delete_jadwal($c);
    echo json_encode($r[0]);

  }


}