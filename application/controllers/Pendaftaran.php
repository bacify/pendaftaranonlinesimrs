<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pendaftaran extends CI_Controller {

public function __construct(){
    parent::__construct();
    $this->load->library('Template');
      $this->load->model('mpendaftaran');
    
   	  
  }

  public function index(){
  	$this->template->load('home');
  }

  public function form_reg(){
  		
  		$value['tipe_layanan']=1;
		$data['layanan']=$this->mpendaftaran->get_layanan($value);

  		$this->template->load('form_reg',$data);
  }

  public function form_nonreg(){
  	$value['tipe_layanan']=1;
	$data['layanan']=$this->mpendaftaran->get_layanan($value);
  	$data['provinsi']=$this->mpendaftaran->get_provinsi();
  	$this->template->load('form_nonreg',$data);
  }


  public function proses_nonreg(){
  	 

      $title=$this->input->post('title');
      $nama=$this->input->post('nama');
      $nama_ayah=$this->input->post('nama_ayah');
      $kelamin=$this->input->post('kelamin');
      $status=$this->input->post('status');
      $pendidikan=$this->input->post('pendidikan');
      $agama=$this->input->post('agama');
      $tempat_lahir=$this->input->post('tempat_lahir');
      $tgl_lahir=$this->input->post('tgl_lahir');
      $alamat=$this->input->post('alamat');
      $provinsi=$this->input->post('provinsi');
      $kota=$this->input->post('kota');
      $kec=$this->input->post('kec');
      $kel=$this->input->post('kel');
      $no_ktp=$this->input->post('no_ktp');
      $no_telp=$this->input->post('no_telp');
      $pekerjaan=$this->input->post('pekerjaan');
      $penanggung_jawab=$this->input->post('penanggung_jawab');
      $telp_penanggung=$this->input->post('telp_penanggung');

      $poli=$this->input->post('poli');
      $tgl_kunjungan=$this->input->post('tgl_kunjungan');
      $jenispasien=$this->input->post('jenispasien');
      $no_anggota=$this->input->post('no_anggota');
      $email=$this->input->post('email');
      $status_kunjugan=1; //pasien baru
       

       $data_pasien['title']=$title;
       $data_pasien['nama']=$nama;
       $data_pasien['tempat_lahir']=$tempat_lahir;
       $t_lahir=explode('-', $tgl_lahir);
       $data_pasien['tanggal_lahir']=$t_lahir[2].'-'.$t_lahir[1].'-'.$t_lahir[0];
       $data_pasien['jenis_kelamin']=$kelamin;
       $data_pasien['alamat']=$alamat;
       $data_pasien['id_provinsi']=$provinsi;
       $data_pasien['id_kota']=$kota;
       $data_pasien['id_kecamatan']=$kec;
       $data_pasien['id_kelurahan']=$kel;
       $data_pasien['no_telp']=$no_telp;
       $data_pasien['no_ktp']=$kel;
       $data_pasien['nama_ortu']=$nama_ayah;
       $data_pasien['pekerjaan']=$pekerjaan;
       $data_pasien['marital']=$status;
       $data_pasien['agama']=$agama;
       $data_pasien['pendidikan']=$pendidikan;
       $data_pasien['nama_penanggung']=$penanggung_jawab;
        $data_pasien['no_penanggung']=$telp_penanggung;
        $tgl_daftar=explode('-', $tgl_kunjungan);
        $data_pasien['tgl_daftar']=$tgl_daftar[2].'-'.$tgl_daftar[1].'-'.$tgl_daftar[0];
       
      
      //insert pasien
        $id_pasien=$this->mpendaftaran->insert_pasien($data_pasien);

        /*INSERT DATA KUNJUNGAN*/
        $t_kunjung=explode('-', $tgl_kunjungan);
        $data_pendaftaran=array();
        $data_pendaftaran['layanan']=$poli;
        $data_pendaftaran['tgl_kunjungan']=$t_kunjung[2].'-'.$t_kunjung[1].'-'.$t_kunjung[0];
        $data_pendaftaran['cara_bayar']=$jenispasien;
        $data_pendaftaran['nomor_anggota']=$no_anggota;
        $data_pendaftaran['email']=$email;
        $data_pendaftaran['status_kunjungan']=$status_kunjugan;
        $data_pendaftaran['pasien_id']=$id_pasien;
        $id_kunjungan=$this->mpendaftaran->insert_pendaftaran($data_pendaftaran);

		$kode=null;
        if($id_kunjungan!=null){
        	$id=substr($id_kunjungan, -3);
        	$kode='R'.date('ymd').sprintf("%03d", $id);
        }
        $result=array();
        if($kode!=null) {
        	$result['status']=true;
        	$result['kode']=$kode;

        	//KODE SEND MAIL
        	
        	if($email!=''){
	        	$pengunjung=array('nama'=>$title.'. '.$nama,
	    						'email'=>$email,
	    						'kode'=>$kode);
	        	$this->sendmail($pengunjung);
        	}
			
        }else{
        	$result['status']=false;
        	$result['kode']=0;
        }           



		

        echo json_encode($result);


  }
  public function proses_reg(){
  	 

       
      $nama=$this->input->post('nama');
      $tempat_lahir=$this->input->post('tempat_lahir');
      $nomr=$this->input->post('nomr');
      

      $poli=$this->input->post('poli');
      $tgl_kunjungan=$this->input->post('tgl_kunjungan');
      $jenispasien=$this->input->post('jenispasien');
      $no_anggota=$this->input->post('no_anggota');
      $email=$this->input->post('email');
      $status_kunjugan=2; //pasien baru
       

       

        /*INSERT DATA KUNJUNGAN*/
        $t_kunjung=explode('-', $tgl_kunjungan);
        $data_pendaftaran=array();
        $data_pendaftaran['layanan']=$poli;
        $data_pendaftaran['no_rm']=$nomr;
        $data_pendaftaran['tgl_kunjungan']=$t_kunjung[2].'-'.$t_kunjung[1].'-'.$t_kunjung[0];
        $data_pendaftaran['cara_bayar']=$jenispasien;
        $data_pendaftaran['nomor_anggota']=$no_anggota;
        $data_pendaftaran['email']=$email;
        $data_pendaftaran['status_kunjungan']=$status_kunjugan;
         
        $id_kunjungan=$this->mpendaftaran->insert_pendaftaran($data_pendaftaran);

        
		$kode=null;
        if($id_kunjungan!=null){
        	$id=substr($id_kunjungan, -3);
        	$kode='M'.date('ymd').sprintf("%03d", $id);
        }
        $result=array();
        if($kode!=null) {
        	$result['status']=true;
        	$result['kode']=$kode;

        	//KODE SEND MAIL
        	
        	if($email!=''){
	        	$pengunjung=array('nama'=>$nama,
	    						'email'=>$email,
	    						'kode'=>$kode);
	        	$this->sendmail($pengunjung);
        	}
			
        }else{
        	$result['status']=false;
        	$result['kode']=0;
        }           



		

        echo json_encode($result);


  }
  public function berhasil(){
  	$nomor=$this->uri->segment(3);
  	$data['nomor']=$nomor;
  	$this->template->load('berhasil',$data);
  }
  public function sendMail($data){
		
		$config['protocol'] = 'smtp';
		$config['smtp_crypto'] = 'tls';
		$config['smtp_host'] = 'mail.slwebid.com';
		$config['smtp_port'] = '587';
		$config['smtp_user'] = 'testing@slwebid.com';
		$config['smtp_pass'] = 'Rahasiabr0';
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['newline'] = "\r\n";
		$config['bcc_batch_mode'] = TRUE;
		$this->email->initialize($config);
		$this->email->from('testing@slwebid.com', 'RSUD RA BASOENI');
		$to=$data['email'];
		$title='Konfirmasi Pendaftaran '.$data['kode'];
		$body='Terima Kasih '.$data['nama'].' telah melakukan pendaftaran.<br>';
		$body.='Nomor Pendaftaran anda '.$data['kode'].'.<br>';
		$body.='tunjukan nomor pendaftaran ketika melakukan kunjungan.<br>';
		$body.='untuk mencetak checklist kelengkapan yang harus dibawa, silahkan klik link dibawah<br>';
		$body.='<a href="https://daftar.slwebid.com/pendaftaran/berhasil/'.$data['kode'].'" > Checklist </a>';
		$this->email->to($to);
		$this->email->subject($title);
				
		//$body = $this->load->view('KirimEmail',$data,TRUE);
		 
		$this->email->message($body);
		if ($this->email->send()) {
			// echo 'Email sent.';
		} else {
			//show_error($this->email->print_debugger());
		}
	}

  public function testmail(){
      $pengunjung=array('nama'=>'Asti',
                  'email'=>'bacicixy@gmail.com',
                  'kode'=>'12345677');
      $this->sendMail($pengunjung);
  }
  public function get_city(){
		$id=$_POST['country_id'];
		$val['province_id']=$id;
		$kota=$this->mpendaftaran->get_city($val);
		$r='';
		foreach($kota as $k){
			$r=$r.'<option value="'.$k->id.'">'.$k->name.'</option>';
		}
		echo $r;
	}
	public function get_district(){
		$id=$_POST['city_id'];

		$val['regency_id']=$id;
		$kota=$this->mpendaftaran->get_district($val);
		 
		$r='';
		foreach($kota as $k){
			$r=$r.'<option value="'.$k->id.'">'.$k->name.'</option>';
		}
		echo $r;
	}
	public function get_village(){
		$id=$_POST['district_id'];
		$val['district_id']=$id;
		$kota=$this->mpendaftaran->get_village($val);
		$r='';
		foreach($kota as $k){
			$r=$r.'<option value="'.$k->id.'">'.$k->name.'</option>';
		}
		echo $r;
	}

}