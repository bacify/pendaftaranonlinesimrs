<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Mainapi extends REST_Controller {

    function __construct()
    {
		
        // Construct the parent class
        parent::__construct();
		$this->load->model('mapi');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->methods['pasien_put']['limit'] = 50; // 50 requests per hour per user/key
        $this->methods['pasien_put']['level'] = 1; // 50 requests per hour per user/key
    
    }
    public function apicheck_get(){

    			$this->set_response([
                    'status' => TRUE,
                    'message' => 'Request API Berhasil Dilakukan'
                    
                ], REST_Controller::HTTP_OK); // NO_CONTENT (200) being the HTTP response code

    }
	public function pasien_put()
    {
       					/*	$nama=$this->input->post('nama');
							$tempat_lahir=$this->input->post('tempat_lahir');
							 $tanggal_lahir=$this->input->post('tanggal_lahir');
							 $tanggal_lahir=>$this->input->post('alamat');
							 $jenis_kelamin=>$this->input->post('jenis_kelamin');
							 $alamat=>$this->input->post('alamat');
							 $id_provinsi=>$this->input->post('id_provinsi');
							 $id_kota=>$this->input->post('id_kota');
							 $id_kecamatan=>$this->input->post('id_kecamatan');
							 $id_kelurahan=>$this->input->post('id_kelurahan');
							 $no_telp=>$this->input->post('no_telp');
							 $no_ktp=>$this->input->post('no_ktp');
							 $alamat_ktp=>$this->input->post('alamat_ktp');
							 $nama_ortu=>$this->input->post('nama_ortu');
							 $pekerjaan=>$this->input->post('pekerjaan');
							 $marital=>$this->input->post('marital');
							 $pendidikan=>$this->input->post('pendidikan');
							 $nama_penanggung=>$this->input->post('nama_penanggung');
							 $alamat_penanggung=>$this->input->post('alamat_penanggung');
							 $no_penanggung=>$this->input->post('no_penanggung');
							 $hubungan_penanggung=>$this->input->post('nama_penanggung');
							 $tgl_daftar=>$this->input->post('tgl_daftar');
		
		if($nama!=null){
			$pasien=array();
			 // $this->some_model->insert( ... );
			if($nama!=null)
				$pasien['nama']=$nama;
			if($tempat_lahir!=null)
				$pasien['tempat_lahir']=$tempat_lahir;
			if($tanggal_lahir!=null)
				$pasien['tanggal_lahir']=$tanggal_lahir;
			if($jenis_kelamin!=null)
				$pasien['jenis_kelamin']=$jenis_kelamin;
			if($alamat!=null)
				$pasien['alamat']=$alamat;
			if($id_provinsi!=null)
				$pasien['id_provinsi']=$id_provinsi;
			if($id_kota!=null)
				$pasien['id_kota']=$id_kota;
			if($id_kecamatan!=null)
				$pasien['id_kecamatan']=$id_kecamatan;
			if($id_kelurahan!=null)
				$pasien['id_kelurahan']=$id_kelurahan;
			if($no_telp!=null)
				$pasien['no_telp']=$no_telp;
			if($no_ktp!=null)
				$pasien['noKTP']=$noKTP;
			if($alamat_ktp!=null)
				$pasien['alamat_ktp']=$alamat_ktp;
			if($nama_ortu!=null)
				$pasien['nama_ortu']=$nama_ortu;
			if($pekerjaan!=null)
				$pasien['pekerjaan']=$pekerjaan;
			if($marital!=null)
				$pasien['marital']=$marital;
			if($pendidikan!=null)
				$pasien['pendidikan']=$pendidikan;
			if($nama_penanggung!=null)
				$pasien['nama_penanggung']=$nama_penanggung;
			if($alamat_penanggung!=null)
				$pasien['alamat_penanggung']=$alamat_penanggung;
			if($no_penanggung!=null)
				$pasien['no_penanggung']=$no_penanggung;
			if($hubungan_penanggung!=null)
				$pasien['hubungan_penanggung']=$hubungan_penanggung;
			if($tgl_daftar!=null)
				$pasien['tgl_daftar']=$tgl_daftar;
			
			
			
			$id=$this->rs_model->insert_pasien($pasien);*/
			$id=1;
			$pasien['id']=$id;
			if($id!=null){
			$this->set_response([
                    'status' => TRUE,
                    'message' => 'Pasien Baru Telah Dibuat',
                    'pasien' => $pasien
                ], REST_Controller::HTTP_OK); // NO_CONTENT (200) being the HTTP response code
				
			}else{
				$this->response([
				'status'=>FALSE,
				'message'=> 'Parameter Salah'], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
				
			}
         
         
	 
		 
        
    }
	
	  public function pasien_get()
    {
        

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
			// Users from a data store e.g. database
				$pasien =  $this->pasien_model->get_pasien();
				
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($pasien)
            {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'Pasien ditemukan',
                    'pasien' => $pasien
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Pasien Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

		//QUERY USER
        $pasien =  $this->pasien_model->get_pasien(array('id'=>$id));

        if (!empty($pasien))
        {
            $this->set_response([
                    'status' => TRUE,
                    'message' => 'Pasien Ditemukan',
                    'pasien' => $pasien
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Pasien Tidak ditemukan'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
	
	 public function pasien_post()
    {
		
		
		$id=$this->post('id');
		$nama=$this->post('nama');
		$alamat=$this->post('alamat');
		$noKTP=$this->post('noKTP');
		if($id===NULL){
			// Invalid id, set the response and exit.
            $this->response([
					'status' => FALSE,
					'message' => 'ID Tidak Ditemukan' 
					], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
		}else{
			$pasien=array();
			$cond['id']=$this->post('id');
			// $this->some_model->update_user( ... );
			if($nama!=null)
				$pasien['nama']=$nama;
			if($alamat!=null)
				$pasien['alamat']=$alamat;
			if($noKTP!=null)
				$pasien['noKTP']=$noKTP;
			 
			$status=$this->pasien_model->update_pasien($pasien,$cond);
			$pasien['id']=$id;
			if($status)
				$this->set_response([
					'status' => True,
					'message' => 'Update Pasien Berhasil',
					'pasien' => $pasien
					] , REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
			else
				$this->set_response([
					'status' => FALSE,
					'message' => 'Update Pasien Gagal',
					'pasien' => $pasien
					], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		}
    }
	
	
	public function pasien_delete()
    {
		 
        $id = (int) $this->delete('id');
		  
        // Validate the id.
        if ($id <= 0)
        {
			 
            // Set the response and exit
            $this->response([
            'id' => $id,
            'status' => FALSE,
            'message' => 'HAPUS GAGAL'
			], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
	 
		$cond['id']=$id ;
        // $this->some_model->delete_something($id);
        $status=$this->pasien_model->delete_pasien($cond);
		if($status)
		$message = [
            'id' => $id,
            'status' => TRUE,
            'message' => 'pasien Berhasil Dihapus'
        ];
		else
		$message = [
            'id' => $id,
            'status' => FALSE,
            'message' => 'pasien Gagal Dihapus'
        ];	
        $this->set_response($message, REST_Controller::HTTP_OK); // NO_CONTENT (200) being the HTTP response code
    }


     public function kunjungan_get()
    {
        

        $tgl = $this->get('tanggal_kunjungan');
        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($tgl === NULL && $id==NULL)
        {
			// Kunjungan from a data store e.g. database
				$kunjungan =  $this->mapi->get_kunjungan();

			//GET PASIEN BARU
				foreach ($kunjungan as $k) {
					if($k->status_kunjungan==1 && $k->pasien_id!=0){
						$c['id']=$k->pasien_id;
						$k->data_pasien=$this->mapi->get_pasien($c)[0];
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='M'.date('ymd',$time).sprintf("%03d", $k->id);
					}else{
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='R'.date('ymd',$time).sprintf("%03d", $k->id);
					}
				}
				
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($kunjungan)
            {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'List kunjungan ditemukan',
                    'kunjungan' => $kunjungan
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Kunjungan Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        if($id!=NULL && $tgl===NULL){

        		if(strlen($id)!=10){
	        			$this->response([
	                    'status' => TRUE,
	                    'message' => 'format ID salah'
	                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        		}
        		$y=substr($id,1,2);
				$m=substr($id,3,2);
				$d=substr($id,5,2);
				if(!(is_numeric($y)&&is_numeric($m)&&is_numeric($d)) ){
					$this->response([
	                    'status' => TRUE,
	                    'message' => 'format ID salah'
	                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
				}

        	// Kunjungan from a data store e.g. database
        		$id_kunjungan=(int)substr($id, -3);
        		$a['right(id,3)']=$id_kunjungan;
        		$a['date(tgl_daftar)']='20'.$y.'-'.$m.'-'.$d;
        		
				$kunjungan =  $this->mapi->get_kunjungan($a);

			//GET PASIEN BARU
				foreach ($kunjungan as $k) {
					if($k->status_kunjungan==1 && $k->pasien_id!=0){
						$c['id']=$k->pasien_id;
						$k->data_pasien=$this->mapi->get_pasien($c)[0];
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='M'.date('ymd',$time).sprintf("%03d", $k->id);
					}
					else{
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='R'.date('ymd',$time).sprintf("%03d", $k->id);
					}
				}
				
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($kunjungan)
            {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'List kunjungan ditemukan',
                    'kunjungan' => $kunjungan
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Kunjungan Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
        if($tgl!=NULL && $id==null){


        	// Kunjungan from a data store e.g. database
        		$id_kunjungan=(int)substr($id, -3);
        		$a['date(tgl_kunjungan)']=$tgl;
        		
				$kunjungan =  $this->mapi->get_kunjungan($a);

			//GET PASIEN BARU
				foreach ($kunjungan as $k) {
					if($k->status_kunjungan==1 && $k->pasien_id!=0){
						$c['id']=$k->pasien_id;
						$k->data_pasien=$this->mapi->get_pasien($c)[0];
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='M'.date('ymd',$time).sprintf("%03d", $k->id);
					}
					else{
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='R'.date('ymd',$time).sprintf("%03d", $k->id);
					}
				}
				
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($kunjungan)
            {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'List kunjungan ditemukan',
                    'kunjungan' => $kunjungan
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Kunjungan Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
            	
        }
        if($tgl!=NULL && $id!=null){
        	
        	if(strlen($id)!=10){
	        			$this->response([
	                    'status' => TRUE,
	                    'message' => 'format ID salah'
	                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        		}
        		$y=substr($id,1,2);
				$m=substr($id,3,2);
				$d=substr($id,5,2);
				if(!(is_numeric($y)&&is_numeric($m)&&is_numeric($d)) ){
					$this->response([
	                    'status' => TRUE,
	                    'message' => 'format ID salah'
	                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
				}
				
        	// Kunjungan from a data store e.g. database
        		$id_kunjungan=(int)substr($id, -3);
        		$a['date(tgl_kunjungan)']=$tgl;
        		$a['right(id,3)']=$id_kunjungan;
				$kunjungan =  $this->mapi->get_kunjungan($a);

			//GET PASIEN BARU
				foreach ($kunjungan as $k) {
					if($k->status_kunjungan==1 && $k->pasien_id!=0){
						$c['id']=$k->pasien_id;
						$k->data_pasien=$this->mapi->get_pasien($c)[0];
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='M'.date('ymd',$time).sprintf("%03d", $k->id);
					}
					else{
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='R'.date('ymd',$time).sprintf("%03d", $k->id);
					}
				}
				
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($kunjungan)
            {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'List kunjungan ditemukan',
                    'kunjungan' => $kunjungan
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Kunjungan Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
            	
        }
        // Find and return a single record for a particular user.

       
    }

    public function kunjunganid_get()
    {
        

			// Kunjungan from a data store e.g. database
				$kunjungan =  $this->mapi->get_kunjungan();

			//GET PASIEN BARU
				foreach ($kunjungan as $k) {
					if($k->status_kunjungan==1 && $k->pasien_id!=0){
						$c['id']=$k->pasien_id;
						$k->data_pasien=$this->mapi->get_pasien($c)[0];
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='M'.date('ymd',$time).sprintf("%03d", $k->id);
					}else{
						$time=strtotime($k->tgl_daftar);
						$k->kode_pendaftaran='R'.date('ymd',$time).sprintf("%03d", $k->id);
					}
				}
				$result=array();
				foreach ($kunjungan as $k) {
					$result[]=$k->kode_pendaftaran;
				}
				
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($result)
            {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'List kunjungan ditemukan',
                    'list_kode' => $result
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Kunjungan Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
      

       
    }

       public function kunjungantotal_get()
    {
        

			// Kunjungan from a data store e.g. database
				$kunjungan =  $this->mapi->get_kunjungan();

			
				$result=count($kunjungan);
				
				
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($result)
            {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'kunjungan ditemukan',
                    'total' => $result
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Kunjungan Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
      

       
    }


	
	
}
