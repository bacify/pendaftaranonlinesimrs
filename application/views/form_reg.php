 
<div class="form-pendaftaran">
	<section class="form-box" >
            <div class="container form-reg">
                
                <div class="row">
                    <div class="col-sm-10  offset-sm-1 col-md-10 offset-md-1 col-lg-10 offset-lg-1 form-wizard">
					
						<!-- Form Wizard -->
                    	<form role="form" action="" method="post">

                    		<h3>Pendaftaran Pasien</h3>
                    		<p>Isi Semua Data untuk melanjutkan kelangkah selanjutnya</p>
							
							<!-- Form progress -->
                    		<div class="form-wizard-steps form-wizard-tolal-steps-4">
                    			<div class="form-wizard-progress">
                    			    <div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4" style="width: 12.25%;"></div>
                    			</div>
								<!-- Step 1 -->
                    			<div class="form-wizard-step active">
                    				<div class="form-wizard-step-icon"><span class="fa fa-user icon-title" aria-hidden="true"></span></div>
                    				<p>Data pasien</p>
                    			</div>
								<!-- Step 1 -->
								
								<!-- Step 2 -->
                    			<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><span class="fa fa-location-arrow icon-title" aria-hidden="true"></span></div>
                    				<p>Data Kunjungan</p>
                    			</div>
								<!-- Step 2 -->
								
								<!-- Step 3 -->
								<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><span class="fa fa-briefcase icon-title" aria-hidden="true"></span></div>
                    				<p>Jenis pasien</p>
                    			</div>
								<!-- Step 3 -->
								
								<!-- Step 4 -->
								<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><span class="fa fa-list-alt icon-title" aria-hidden="true"></span></div>
                    				<p>Konfirmasi Data</p>
                    			</div>
								<!-- Step 4 -->
                    		</div>
							<!-- Form progress -->
                    		
							
							<!-- Form Step 1 -->
                    		<fieldset>

                    		    <h4>informasi Pasien: <span>Step 1 - 4</span></h4>
								
								 
                                <div class="form-group">
                    			    <label class="control-label">Nama Pasien: <span>*</span></label>
                                    <input type="text" name="nama" placeholder="Nama Pasien" class="form-control required">
                                </div>
								
								  
									<!-- Text input-->
									<div class="form-group">
									  <label class="  control-label" for="textinput">Tanggal Lahir</label>  
									 
									  <input id="tgl_lahir" name="tgl_lahir" placeholder="20-12-2000" class="form-control required" type="text" value="" required>
									   
									</div>

									 
								<!-- Textarea -->
								<div class="form-group">
								  <label class="control-label" for="nomr">No MR</label>
								  <input id="nomr" name="nomr" placeholder="012345" class="form-control required" type="text" >   
								  <small class="form-text text-muted">*nomor yang anda dapatkan ketika pernah berkunjung</small> 
								 </div>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-next btn-nomr">Next</button>
                                </div>
                            </fieldset>
							<!-- Form Step 1 -->

							<!-- Form Step 2 -->
                            <fieldset>

                                <h4>Informasi Pendaftaran : <span>Step 2 - 4</span></h4>
								<div class="form-group">
								  <label class="control-label" for="poli">Poli Tujuan</label>  
								 
								  <select class="form-control" id="poli" name="poli" required>
								  <option disabled selected required></option>
								     <?php
								    if($layanan!==''){
								      foreach($layanan as $d){
								        ?>
								       <option value="<?php echo $d->id; ?>"><?php echo $d->nama_layanan; ?></option>
								      <?php
								      }
								    }else{
								      ?>
								    <option value="0"> - </option>
								      <?php
								    }
								    ?>
								    
								  </select>

								  
								</div>

								<div class="form-group">
								  <label class="control-label" for="textinput">Tanggal Kunjungan</label>  
								  <input id="tgl_daftar" name="tgl_daftar" placeholder="20-01-2019" class="form-control required" type="text"  >
								 <small class="form-text text-info"><b>*kosongi jika tidak ada</b> </small>
								</div>
								<br/>
								 <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>
							<!-- Form Step 2 -->

							<!-- Form Step 3 -->
                            <fieldset>

                                <h4>Jenis Pasien: <span>Step 3 - 4</span></h4>
								<div class="form-group">
								  <label class="col-md-3 control-label" for="textinput">Kelompok Pasien</label>  
								
								  <select class="form-control" id="jenispasien" name="jenispasien" required>
								  	<option value="99">Umum</option>
								  	<option value="1">BPJS</option>
								  	<option value="3">BPJS TK</option>
								  	
								  </select>
								 
								</div>
								<div class="form-group">
								  <label class="control-label" for="textinput">Nomor Anggota</label>  
								  <input  name="no_anggota" placeholder="0" class="form-control" type="text" >
								 	<small class="form-text text-muted">*kosongi jika tidak ada </small>
								</div>
								 
								<br/>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next btn-confirmation-reg">Next</button>
                                </div>
                            </fieldset>
							<!-- Form Step 3 -->
							
							<!-- Form Step 4 -->
							<fieldset>

                                <h4>Pastikan Data Anda Sesuai Sebelum Di Proses : <span>Step 4 - 4</span></h4>
								<div style="clear:both;"></div>
								<div class="form-group">
								  <label class="control-label" for="textinput">Masukkan Email</label>  
								  <input  name="email" placeholder="example@mail.com" class="form-control required" type="email" required>
								 	<small class="form-text text-muted">*digunakan untuk mengirim konfirmasi pendaftaran </small>
								</div>
								<div class="data-konfirmasi">
									 <p> <u> Informasi Pasien </u></p>
									 <p>Nama : <strong> <span id="knama"></span> </strong> </p>
									 <p>Tanggal Lahir : <strong> <span id="ktgl_lahir"></span> </strong></p>
									 <p>NoMR : <strong> <span id="knomr"></span> </strong></p>
									 <br>
									 <p> <u> Data Kunjungan </u></p>
									 <p>Poli Tujuan : <strong><span id="kpoli"></span></strong> </p>
									 <p>Tanggal Kunjungan :<strong> <span id="ktgl_kunjungan"></span> </strong></p>
									 <br>
									 <p> <u> Kelompok Pasien </u></p>
									 <p>Jenis pasien :<strong> <span id="kjenis_pasien"></span> </strong></p>
									 <p>No Anggota : <strong><span id="kno_anggota"></span></strong> </p>
								</div>
								 <br>
								 <p class="text-muted">* Pastikan Data sesuai</p>

                    			 
								 
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" id="btn-reg-submit" class="btn btn-submit">Submit</button>
                                </div>
                            </fieldset>
							<!-- Form Step 4 -->
                    	
                    	</form>
						<!-- Form Wizard -->
                    </div>
                </div>
                    
            </div>
        </section>
    </div>