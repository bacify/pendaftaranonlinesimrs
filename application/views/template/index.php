 <?php 
 date_default_timezone_set('Asia/Jakarta');

 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" >
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" >
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/form.css" >
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/home.css" >
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/footer.css" >
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/slick.css" >
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/slick-theme.css" >

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
 <noscript>JavaScript is off. Please enable to view full site.</noscript>

 <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/gif">


</head>
<body>
<div class="se-pre-con"></div>
	 	<?php 
          	  /*
				   * Variabel $contentnya diambil dari libraries template.php
				   * (application/libraries/template.php)
				   * */

            echo $header; 
        ?>
      	<div class="content" >
		<?php 	echo $content;  ?>
		</div>
		<footer>
		<?php
			echo $footer; 
		?>
		</footer>



<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js" ></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui.min.js" ></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
<script src="<?php echo base_url();?>assets/js/slick.min.js" ></script>


<script src="<?php echo base_url();?>assets/js/main.js" ></script>
<script src="<?php echo base_url();?>assets/js/form.js" ></script>


  




</body>
</html>