<?php  date_default_timezone_set('Asia/Jakarta');

?>
<div id="header">
  <!-- As a heading -->
<nav class="navbar navbar-expand-md fixed-top navbar-light bg-light navhead">
  <a href="<?php echo base_url();?>"><span class="navbar-brand mb-0 h1">Pendaftaran Online RSUD RA BASOENI</span></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>

			  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item">
			       		<a class="nav-link" href="http://rsudrabasuni.mojokertokab.go.id/" title="Website RSUD BASOENI"> <span class="fa fa-home fa-lg"></span></a>
			     	</li> 
				      <li class="nav-item">
				        <a class="nav-link" href="mailto:rsud.basoeni@gmail.com" title="Email RSUD BASOENI"> <span class="fa fa-envelope-o fa-lg"></span></a>      
				       </li>
				      <li class="nav-item">
				        <a class="nav-link" href="https://goo.gl/maps/mfbLRFUnjH12" title="Peta RSUD BASOENI"> <span class="fa fa-map fa-lg"></span></a>
				       </li>
				           
			    </ul>
			    
			  </div>
		   
		   
</nav>

</div>
<?php 
$jadwal=$this->mpendaftaran->get_jadwal();

?>

<!--Carousel Wrapper-->
 <div class="container-jadwal">
 	<div class="jadwal-title"><strong><u>Jadwal Layanan</u></strong></div>
 <div id="jadwal-layanan">
 		<div class="card senin"  >     		
			   	<div class="card-body">
			    <h5 class="card-title"><u>Senin</u></h5>
			    <p class="card-text">
			    <ul >
					  <?php  					  
					  $x=0;
					  foreach ($jadwal as $j) {
					  	if($j->day==1){
					  		echo '<li>Poli '.$j->nama_layanan.'</li>';
					  		$x++;
					  	}
					  }
					  if($x==0)
					  	echo '<li>   </li>';
					  ?>					  
				</ul>
			    </p>
			  	</div>
		</div>
		 <div class="card selasa"   >     		
			   	<div class="card-body">
			    <h5 class="card-title"><u>Selasa</u></h5>
			    <p class="card-text">
			    <ul >
					 <?php  					  
					  $x=0;
					  foreach ($jadwal as $j) {
					  	if($j->day==2){
					  		echo '<li>Poli '.$j->nama_layanan.'</li>';
					  		$x++;
					  	}
					  }
					  if($x==0)
					  	echo '<li>  </li>';
					  ?>
				</ul>
				</p>
			    
			  	</div>
		 </div>
		<div class="card rabu"   >     		
			   	<div class="card-body">
			    <h5 class="card-title"><u>Rabu</u></h5>
			    <p class="card-text">
			    <ul >
					<?php  					  
					  $x=0;
					  foreach ($jadwal as $j) {
					  	if($j->day==3){
					  		echo '<li>Poli '.$j->nama_layanan.'</li>';
					  		$x++;
					  	}
					  }
					  if($x==0)
					  	echo '<li>  </li>';
					  ?>
				</ul>
				</p>
			    
			  	</div>
		 	</div>
		 <div class="card kamis"    >     		
			   	<div class="card-body">
			    <h5 class="card-title"><u>kamis</u></h5>
			    <p class="card-text"><ul >
					<?php  					  
					  $x=0;
					  foreach ($jadwal as $j) {
					  	if($j->day==4){
					  		echo '<li>Poli '.$j->nama_layanan.'</li>';
					  		$x++;
					  	}
					  }
					  if($x==0)
					  	echo '<li>   </li>';
					  ?>
				</ul></p>
			    
			  	</div>
		 	</div>
		 <div class="card jumat"    >     		
			   	<div class="card-body">
			    <h5 class="card-title"><u>Jumat</u></h5>
			    <p class="card-text"><ul >
					<?php  					  
					  $x=0;
					  foreach ($jadwal as $j) {
					  	if($j->day==5){
					  		echo '<li>Poli '.$j->nama_layanan.'</li>';
					  		$x++;
					  	}
					  }
					  if($x==0)
					  	echo '<li>   </li>';
					  ?>	
				</ul></p>
			     
			  	</div>
		 	</div>
		 <div class="card sabtu"   >     		
			   	<div class="card-body">
			    <h5 class="card-title"><u>Sabtu</u></h5>
			    <p class="card-text"><ul >
					<?php  					  
					  $x=0;
					  foreach ($jadwal as $j) {
					  	if($j->day==6){
					  		echo '<li>Poli '.$j->nama_layanan.'</li>';
					  		$x++;
					  	}
					  }
					  if($x==0)
					  	echo '<li>   </li>';
					  ?>	
				</ul></p>
			     
			  	</div>
		 	</div>
</div>
 
 </div>
<!--/.Carousel Wrapper-->

 