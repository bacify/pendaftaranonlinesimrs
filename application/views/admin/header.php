<?php  date_default_timezone_set('Asia/Jakarta');

?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <a href="<?php echo base_url();?>"><span class="navbar-brand mb-0 h1">Administrator</span></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url('adminrs/jadwal_poli');?>">Jadwal Poli <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('adminrs/checklist');?>">Checklist Persyaratan <span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </div>


  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item">
			       		 <a class="nav-link" href="<?php echo base_url('adminrs/logout');?>">Logout <span class="sr-only"> </span></a>
			     	</li> 
				           
			    </ul>
			    
			  </div>
</nav>
 