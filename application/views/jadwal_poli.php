<div class="container-fluid main"> 

<div class="card" >
  <div class="card-heading">
  	<div class="text-center"> <h3>Jadwal Poli</h3></div>

    <div><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#ModalaAdd"><span class="fa fa-plus"></span> Tambah Jadwal</button>



    </div>
  </div>
  <div class="card-body">
 
 


            <div class="table-responsive">
              <table id="table-jadwal" >
              	<thead>
					<tr><th>No</th>
                    <th>Hari</th>
                    <th>Nama Poli</th>
                    <th>Tindakan</th>
					</tr>
					</thead>
					<tbody id="showdata">
					</tbody>
              </table>
          </div>
     
  </div>


</div>
 

</div>


<!-- MODAL ADD -->
        <div class="modal fade" id="ModalaAdd"  tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
               
                <h4 class="modal-title" id="myModalLabel">Tambah Jadwal</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">
 
                     <div class="form-group">
                        <label class="control-label col-xs-3" >Hari</label>
                        <div class="col-xs-9">
                        	 <input name="id" id="id_jadwal" class="form-control" type="hidden" value="">
                            <select class="form-control" id="hari" name="hari" style="width:335px;" >
                               
                              <?php for($a=1;$a<=count($day);$a++){
                                echo '<option value="'.$a.'">'.$day[$a].'</option>';
                              }
                              ?>
                            
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Poli</label>
                        <div class="col-xs-9">
                            <select class="form-control" id="poli" name="poli" style="width:335px;" >
                            
                              <?php foreach($layanan as $p){
                                echo '<option value="'.$p->id.'">'.$p->nama_layanan.'</option>';
                              }
                              ?>
                            
                            </select>
                        </div>
                    </div>                  
                      
 
                </div>
 
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" id="btn_simpan">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>
<!--END MODAL ADD-->

<script type="text/javascript">
 

$(document).ready(function() {

    var tabel=$('#table-jadwal').DataTable({
	    	dom: 'Bfrtip',
		    buttons: [
		        'copy', 'excel', 'csv'
		    ],
        "ajax": {
            url : "<?php echo site_url("adminrs/view_jadwal") ?>",
            type : 'GET'
        },

    });



    $('#ModalaAdd').on('shown.bs.modal',function(e){
     
      
    })

    //SHOW DATA
    $('#showdata').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('adminrs/get_jadwal')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                   
                    $('#ModalaAdd').modal('show');
                     $('#id_jadwal').val(data.id_jadwal);
                    if(data.day!=null)
                        $('#hari option[value='+data.day+']').prop('selected', true);  // To select via value
                    else
                        $('#hari option')[0].selected = true;

                    if(data.layanan_id!=null)
                        $('#poli option[value='+data.layanan_id+']').prop('selected', true);  // To select via value
                    else
                        $('#poli option')[0].selected = true;

               
                }
            });
            return false;
        });


    //Simpan Barang
    $('#btn_simpan').on('click',function(){
            
            
            var id_jadwal=$('#id_jadwal').val();
            var hari=$('#hari').val();
            var poli=$('#poli').val();
            
             
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('adminrs/simpan_jadwal')?>",
                dataType : "JSON",
                data : {id:id_jadwal , hari:hari, poli:poli },
                success: function(data){
                  	console.log('aaa');
                  	console.log(data);
                    $('#id_jadwal').val('');
                    
                    $('#hari option')[0].selected = true;
                    $('#poli option')[0].selected = true;

                    $('#ModalaAdd').modal('hide');
                    tabel.ajax.reload();
                    alert(data);
                }
            });
            return false;
        });


      //GET HAPUS
    $('#showdata').on('click','.item_hapus',function(){

      var id=$(this).attr('data');
           if(confirm('Data Akan dihapus, anda yakin?')){
                                 
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('adminrs/hapus_jadwal')?>",
                    dataType : "JSON",
                    data : {id:id},
                    success: function(data){
                      	
                        tabel.ajax.reload();

                    }
                });
               
           }

            return false;
        });
 


});

</script>