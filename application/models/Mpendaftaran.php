<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mpendaftaran extends CI_Model {



	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

         
         public function get_provinsi($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('provinces');
                return $query->result();
        }
        public function get_city($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('regencies');
                return $query->result();
        }
        public function get_district($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('districts');
                return $query->result();
        }
        public function get_village($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('villages');
                return $query->result();
        }
        public function get_layanan($data=NULL)
        {   
                if($data!=NULL)
                    $this->db->where($data);
                $query = $this->db->get('t_layanan');
                return $query->result();
        }

        public function insert_pasien($data){
                $query = $this->db->insert('t_pasien',$data);

                return $this->db->insert_id();
        }
        public function insert_pendaftaran($data){
                $query = $this->db->insert('t_kunjungan',$data);

                return $this->db->insert_id();
        }
         public function get_jadwal($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $this->db->join('t_layanan','t_layanan.id=jadwal_poli.layanan_id');
                $this->db->order_by('day,id_jadwal','asc');
                $query = $this->db->get('jadwal_poli');
                return $query->result();
        }

       
}
