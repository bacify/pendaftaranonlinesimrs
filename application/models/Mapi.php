<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mapi extends CI_Model {
	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

         public function get_kunjungan($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('t_kunjungan');
                return $query->result();
        }
          public function get_pasien($data=null)
        {       

                if($data!=null){
                    $this->db->where($data);
                }     
                $query = $this->db->get('t_pasien');
                return $query->result();
        }

}
